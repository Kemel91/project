<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\CategoryController;

Route::get('/', [CategoryController::class,'index'])->name('home_index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/profile/update_photo', 'ProfileController@updatePhoto')->name('profile.update_photo');
Route::get('/profile', 'ProfileController@index')->name('profile.index');
//Route::get('/category','CategoryController@index')->name('category.index');
Route::get('/item/{id}','ItemController@show')->name('item.show');
//Route::get('/category/create','CategoryController@create')->name('category.create');
Route::resource('category', 'CategoryController') -> names('category');
Route::get('/shop/create','ShopController@create') -> name('shop.create');
Route::post('/shop/store','ShopController@store') -> name('shop.store');
Route::get('/shop_items/create','ShopItemsController@create') -> name('shopitems.create');
Route::get('/shop_items/{id}','ShopItemsController@show') -> name('shopitems.show');
Route::get('/order', 'OrderController@index')->name('order.index');
Route::get('/order/{id}', 'OrderController@show')->name('order.show');
Route::get('/order/put/{id}', 'OrderController@addOrderItem')->name('order.add');
