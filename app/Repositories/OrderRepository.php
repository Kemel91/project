<?php

namespace App\Repositories;

use App\Models\Order as Model;

/**
 * Class OrderRepository
 * @package App\Repositories
 */
class OrderRepository extends CoreRepository
{

    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Выводим название магазина через отношение
     * @param int $id
     * @return mixed
     */
    public function getNameShopByOrderId(int $id)
    {
        $shop = $this->startCondition()
            ->select('id', 'shop_id')
            ->where('id', $id)
            ->with('shop:id,title')
            ->firstOrFail();
        return $shop;
    }

    /**
     * Получаем номер заказа
     * если нет, то создаем новый
     * @param $user_id
     * @param $shop_id
     * @return mixed
     */
    public function getOrderId($user_id, $shop_id)
    {
        $order_id = $this->startCondition()
            ->firstOrCreate(['user_id' => $user_id, 'shop_id' => $shop_id]);
        return $order_id;
    }

    /**
     * Просмотр списков заказов
     * @param int $user_id
     * @return mixed
     */
    public function getOrderShops(int $user_id)
    {
        $orders = $this->startCondition()
            ->select('id', 'shop_id')
            ->where('user_id', $user_id)
            ->with('shop:id,title')
            ->withCount('orderItems')
            ->get();
        return $orders;
    }

    /**
     * Получить предмет из корзины
     * @param int $shop_item_id
     * @param int $shop_id
     * @param int $user_id
     * @return mixed
     */
    public function getItemOrder(int $shop_item_id, int $shop_id, int $user_id)
    {
        $order = $this->startCondition()
            ->where('shop_item_id', $shop_item_id)
            ->where('shop_id', $shop_id)
            ->where('user_id', $user_id)
            ->first();
        return $order;
    }
}
