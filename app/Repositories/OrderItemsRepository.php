<?php

namespace App\Repositories;

use App\Models\OrderItems as Model;

/**
 * Class OrderItemsRepository
 * @package App\Repositories
 */
class OrderItemsRepository extends CoreRepository
{

    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Выводим список товаров из корзины
     * @param int $id
     * @return mixed
     */
    public function getItemsByOrderId(int $id)
    {
        return $this->startCondition()
            ->select('id', 'order_id', 'shop_item_id', 'item_id', 'count')
            ->where('order_id', $id)
            ->with('items:id,title')
            ->with('shopItems:id,price')
            ->paginate();
    }
    /**
     * Добавление товара в корзину
     *
     * @param [int] $order_id
     * @param [int] $shop_item_id
     * @param [int] $item_id
     * @return object
     */
    public function addItem($order_id, $shop_item_id, $item_id)
    {
        $orderItem = $this->startCondition()
            ->firstOrNew([
                'order_id' => $order_id,
                'shop_item_id' => $shop_item_id,
                'item_id' => $item_id
            ]);
        $orderItem->count = ($orderItem->count + 1);
        $orderItem->save();
        return $orderItem;
    }
}
