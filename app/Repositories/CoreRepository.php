<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CoreRepository
 * @package App\Repositories
 */
abstract class CoreRepository {

    /**
     * @var \Illuminate\Contracts\Foundation\Application|mixed
     */
    protected $model;

    public function __construct()
    {
        $this -> model = app($this -> getModelClass());
    }

    /**
     * Undocumented function
     *
     * @return mixed
     */
    abstract protected function getModelClass();

    /**
     * @return \Illuminate\Contracts\Foundation\Application|mixed
     */
    protected function startCondition() {
        return clone $this -> model;
    }
}
