<?php
namespace App\Repositories;

use App\Models\Item as Model;

/**
 * Class ItemRepository
 * @package App\Repositories
 */
class ItemRepository extends CoreRepository{

    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получить товар по ID
     * @param int $id
     * @return mixed
     */
    public function getItem(int $id) {
        return $this -> startCondition() -> find($id);
    }
}
