<?php
namespace App\Repositories;

use App\Models\Category as Model;

/**
 * Class CategoryRepository
 * @package App\Repositories
 */
class CategoryRepository extends CoreRepository{

    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получить категорию по ID
     * @param int $id
     * @return mixed
     */
    public function getCategory(int $id) {
        return $this -> startCondition() -> findOrFail($id);
    }

    /**
     * Получаем список всех категорий
     * @return mixed
     */
    public function getCategories() {
        return $this -> startCondition() -> all();
    }

    /**
     * Создаем модель и сохраняем
     * @param $data
     * @return bool
     */
    public function createCategory($data) {
        return (new Model($data)) -> save();
    }
}
