<?php
namespace App\Repositories;

use App\Models\ShopItems as Model;

/**
 * Class ShopItemsRepository
 * @package App\Repositories
 */
class ShopItemsRepository extends CoreRepository{

    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получить товар по ID из магазина
     * @param int $id
     * @return mixed
     */
    public function getShopItem(int $id) {
        return $this -> startCondition() -> find($id);
    }

    /**
     * Добавляем товар в резерв магазина
     * и убирает из остатка
     * @param $item
     * @return bool
     */
    public function addReserveShopItem($item) {
        $item -> reserved += 1;
        $item -> count -= 1;
        return $item -> save() ?: false;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getListShopItems(int $id) {
        $list = $this -> startCondition() -> where('shop_id',$id) -> with('getItem') -> paginate(10);
        return $list;
    }
}
