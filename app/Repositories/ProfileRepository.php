<?php
namespace App\Repositories;

use App\Models\Profile as Model;

/**
 * Class ProfileRepository
 * @package App\Repositories
 */
class ProfileRepository extends CoreRepository{

    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получить пользователя по ID
     * @param int $id
     * @return mixed
     */
    public function getProfile(int $id) {
        return $this -> startCondition() -> find($id);
    }

    /**
     * Получить список магазинов пользователя
     * @param int $id
     * @return mixed
     */
    public function getShops(int $id) {
        return $this -> getProfile($id) -> with('shops');
    }
}
