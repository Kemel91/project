<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = [
        'title',
        'slug',
        'created_at',
        'updated_at',
        'description'
    ];
    /**
     * Получаем список магазинов по id категории
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function getShops() {
        return $this -> hasMany(Shop::class);
    }

    /**
     * Получить пользователя магазина
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function getUser() {
        return $this -> hasMany(User::class);
    }
}
