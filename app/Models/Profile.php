<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'users';

    public function Shops() {
        return $this -> hasMany(Shop::class,'user_id');
    }
}
