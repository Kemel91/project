<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = [
        'user_id',
        'category_id',
        'title',
        'slug',
        'description',
        'created_at',
        'updated_at'
    ];
    public function orders()
    {
         return $this->hasMany(Order::class);
    }
}
