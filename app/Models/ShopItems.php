<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopItems extends Model
{
    public function items()
    {
        return $this->hasOne(Item::class, 'id', 'shop_item_id');
    }
    public function getItem() {
        return $this -> hasOne(Item::class,'id','item_id') -> select('id','title');
    }
}
