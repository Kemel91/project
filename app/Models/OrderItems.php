<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $table = 'order_items';

    protected $fillable = [
        'order_id',
        'shop_item_id',
        'item_id',
        'count',
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function items() {
        return $this -> hasOne(Item::class,'id','item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shopItems() {
        return $this -> hasOne(ShopItems::class,'id','shop_item_id');
    }
}
