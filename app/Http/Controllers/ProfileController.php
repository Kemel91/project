<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProfileUploadPhoto;
use App\Services\ProfileService;
use App\Repositories\ProfileRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class ProfileController
 * @package App\Http\Controllers
 */
class ProfileController extends Controller
{
    /**
     * @var ProfileService
     */
    protected $service;

    /**
     * @var ProfileRepository
     */
    protected $repository;

    /**
     * Доступ только для авторизованных
     * ProfileController constructor.
     * @param ProfileService $profile
     * @param ProfileRepository $repository
     */
    public function __construct(ProfileService $profile, ProfileRepository $repository)
    {
        $this->middleware('auth');
        $this->service = $profile;
        $this->repository = $repository;
    }

    /**
     * Главная страница профиля
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->getUser();
        $avatar = $this->service->getPhoto($user->id);
        $shops = $this->repository->getShops($user->id);
        return view('profile.index', compact('user', 'avatar', 'shops'));
    }

    /**
     * Загрузить фото на аватар
     *
     * @param ProfileUploadPhoto $request
     * @return \Illuminate\Http\Response
     */
    public function updatePhoto(ProfileUploadPhoto $request)
    {
        if ($this->service->uploadPhoto($this->getUser()->id, $request)) {
            return redirect()
                ->route('profile.index')
                ->with(['success' => 'Фотография обновлена']);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка сохранения'])
                ->withInput();
        }
    }

    /**
     * User information
     *
     * @return Illuminate\Support\Facades\Auth
     */
    public function getUser()
    {
        return Auth::user();
    }
}
