<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryCreate as CategoryCreateRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;

/**
 * Class CategoryController
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * CategoryController constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Вывод категорий магазинов
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryRepository->getCategories();
        return view('category.index', compact('categories'));
    }

    /**
     * Получаем список магазинов в заданной категории
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $category = $this->categoryRepository->getCategory($id);
        $shops = $category->getShops;
        return view('category.show', compact('shops', 'category'));
    }

    /**
     * Вывод страницы с созданием категории
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Category::class);
        return view('category.create');
    }

    /**
     * Сохранение новой категории
     * @param CategoryCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryCreateRequest $request)
    {
        $data = $request->validated();
        $category = $this -> categoryRepository -> createCategory($data);
        if ($category) {
            return redirect()
                ->route('category.index')
                ->with(['success' => 'Категория добавлена']);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка сохранения'])
                ->withInput();
        }
    }
}
