<?php

namespace App\Http\Controllers;

use App\Repositories\ItemRepository;
use App\Services\ItemService;

/**
 * Class ItemController
 * @package App\Http\Controllers
 */
class ItemController extends Controller
{

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * ItemController constructor.
     * @param ItemRepository $itemRepository
     */
    public function __construct(ItemRepository $itemRepository)
    {
        $this -> itemRepository = $itemRepository;
    }

    /**
     * @param ItemService $service
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(ItemService $service, int $id)
    {
        $item = $this->itemRepository->getItem($id);
        if (is_null($item)) abort(404);
        $image = $service -> getRandomImage();
        return view('item.show', compact('item', 'image'));
    }

}
