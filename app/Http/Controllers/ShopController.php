<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Models\Shop;
use App\Http\Requests\ShopCreate;

/**
 * Class ShopController
 * @package App\Http\Controllers
 */
class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Создание нового магазина
     * @param App\Repositories\CategoryRepository $repository
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryRepository $repository)
    {
        $categories = $repository -> getCategories();
        return view('shop.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ShopCreate  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopCreate $request)
    {
        $data = $request -> validated();
        $data['user_id'] = \Auth::id();
        $data['slug'] = str_slug($data['title']);
        $shop = new Shop();
        $shop -> fill($data) -> save();
        if ($shop) {
            return redirect()
            -> route('profile.index')
            -> with(['success' => 'Магазин добавлен']);
        } else {
            return back() -> withErrors(['msg' => 'Ошибка сохранения']) -> withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
