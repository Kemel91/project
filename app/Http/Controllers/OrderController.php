<?php

namespace App\Http\Controllers;

use App\Repositories\OrderRepository;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{

    private $orderRepository;

    /**
     * OrderController constructor.
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->middleware('auth');
        $this->orderRepository = $orderRepository;
    }

    /**
     * Выводим список заказов
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order_shops = $this->orderRepository->getOrderListShops();
        return view('order.index', compact('order_shops'));
    }

    /**
     * Выводим список элементов корзины
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return view('order.show', $this -> orderRepository -> getOrderItems($id));
    }

    /**
     * Добавление товара в корзину
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addOrderItem(int $id)
    {
        $shop_id = $this->orderRepository->addOrderItem($id);
        return $shop_id ?
            redirect()->route('shopitems.show', $shop_id)->with(['success' => 'Товар добавлен'])
            : back()->withErrors(['msg' => 'Ошибка добавления'])->withInput();
    }
}
