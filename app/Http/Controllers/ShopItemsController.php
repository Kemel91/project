<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shop;
use App\Repositories\ShopItemsRepository;

/**
 * Class ShopItemsController
 * @package App\Http\Controllers
 */
class ShopItemsController extends Controller
{
    /**
     * @var ShopItemsRepository
     */
    protected $repository;

    /**
     * ShopItemsController constructor.
     * @param ShopItemsRepository $repository
     */
    public function __construct(ShopItemsRepository $repository)
    {
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shop.items_add');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shop = Shop::findOrFail($id);
        $user_id = \Auth::id();
        $paginator = $this -> repository -> getListShopItems($id);
        return view('shop.items_show', compact('paginator', 'shop', 'user_id'));
    }
}
