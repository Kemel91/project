<?php


namespace App\Services;

/**
 * Class ItemService
 * @package App\Services
 */
class ItemService
{
    /**
     * @return string
     */
    public function getRandomImage()
    {
        return 'https://picsum.photos/300';
    }
}
