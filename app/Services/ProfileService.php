<?php
namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Storage;

/**
 * Class ProfileService
 * @package App\Services
 */
class ProfileService {

    /**
     * Функция загрузки фото
     * @param int $user_id
     * @param Request $request
     * @return bool
     */
    public function uploadPhoto(int $user_id, Request $request) {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = $user_id .'.'.$image -> getClientOriginalExtension();
            Storage::putFileAs('public/avatars', $image, $name);
            return true;
          } else {
            Log::error('No hasFile image',['class' => get_class($this)]);
          }
        return false;
    }

    /**
     * Получение ссылки на аватар
     * @param int $user_id
     * @return URL|string
     */
    public function getPhoto(int $user_id) {
        $path = 'avatars/' . $user_id . '.png';
        return Storage::disk('public') -> exists($path) ? Storage::url($path) : $this -> getNoPhoto();
    }

    /**
     * Get random web images Fakeimg
     * @return string
     */
    public function getNoPhoto() {
        return 'https://fakeimg.pl/200/?text=NoPhoto&font=lobster';
    }
}
