<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        //
        $categories = array();
        $titles = ['Без категории','Универсальный','Хозяйственный','Бакалея','Бытовая химия'];
        $i = 0;
        foreach ($titles as $shop) {
            $categories[] = [
                'title' => $shop,
                'slug' => Str::slug($shop),
                'description' => $faker -> text(),
                'parrent_id' => 0
            ];
            $i++;
        }
        \DB::table('categories') -> insert($categories);
    }
}
