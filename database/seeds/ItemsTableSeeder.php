<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $items = array();
        for ($i = 0; $i < 50; $i++) {
            $title = $faker -> unique() -> name;
            $items[] = [
                'title' => $title,
                'slug' => Str::slug($title),
                'user_id' => rand(1,10),
                'description' => $faker -> text(),
                'created_at' => Carbon::now()
            ];
        }
        \DB::table('items') -> insert($items);
    }
}
