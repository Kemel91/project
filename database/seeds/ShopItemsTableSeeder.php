<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ShopItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = array();
        for ($i = 0; $i < 250; $i++) {
            $items[] = [
                'shop_id' => mt_rand(1,10),
                'item_id' => mt_rand(1,50),
                'user_id' => rand(1,10),
                'count' => mt_rand(1,100),
                'price' => (int)round((mt_rand(1,10000)/10),2),
                'created_at' => Carbon::now()
            ];
        }
        \DB::table('shop_items') -> insert($items);
    }
}
