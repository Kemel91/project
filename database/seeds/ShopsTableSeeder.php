<?php
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Generator as Faker;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $add = array();
        $shops = [
            'Магнит' => 2, 
            'Лента' => 2, 
            'ХозМаг' => 3, 
            'Магнит Косметик' => 5, 
            'Елисей' => 4,
            'Колосок' => 2,
            'Окей' => 3,
            'Хозтовары' => 1,
            'Красное & Белое' => 2,
            'Рубль Бум' => 5
        ];
        foreach ($shops as $title => $cat_id) {
            $add[] = [
                'title' => $title,
                'category_id' => $cat_id,
                'slug' => str_slug($title),
                'user_id' => rand(1,10),
                'description' => $faker -> text(),
                'created_at' => Carbon::now()
            ];
        }
        \DB::table('shops') -> insert($add);
    }
}
