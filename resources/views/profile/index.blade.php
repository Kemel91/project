@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ваш профиль</div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="row justify-content-center dark">
                            Ваша фотография обновленна
                        </div>
                    @endif
                    @error('msg')
                        <div class="row justify-content-center dark">
                                        <strong>{{ $message }}</strong>
                        </div>
                    @enderror
                    <div class="row">
                    <div class="col-md-4">
                        <form action="{{ route('profile.update_photo') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="exampleFormControlFile1">
                                <img src="{{ $avatar }}" alt="Photo" width="200" height="200" class="img-thumbnail">
                            </label>
                            <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                            <button type="submit" class="btn btn-primary">
                                    Обновить фото
                            </button>
                        </div>
                        </form>

                    </div>
                    <div class="col-md-8">
                        <li>{{ $user -> name }}</li>
                        <li>{{ $user -> email }}</li>
                    </div>
                    </div>

                    <div class="row justify-content-center">

                            <h3>Мои магазины</h3>
                            <div class="row justify-content-center">
                            @forelse ($shops as $shop)
                                <div class="col-md-4">
                                    <div class="card" style="margin-bottom:10px">
                                        <div class="card-header text-center">
                                            <a href="{{ route('shopitems.show', $shop->id) }}">
                                                {{ $shop -> title }}
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            {{ $shop -> description }}
                                        </div>
                                    </div>
                                </div>
                            @empty
                            <div class="col-md-12">
                                    <div class="card" style="margin-bottom:10px">
                                    Нет добавленных магазинов
                                </div>
                            </div>
                            @endforelse
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="row">
                <a class="btn btn-primary" href="{{ route('shop.create') }}">Добавить магазин</a>
            </div>
        </div>
    </div>
</div>
@endsection
