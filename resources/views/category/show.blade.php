@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="{{ route('category.index') }}">Категории</a> | {{ $category -> title }}</div>

                <div class="card-body">
                    @if (!$shops -> isEmpty())
                    <div class="row justify-content-center">
                        @foreach ($shops as $shop)
                            <div class="col-md-4">
                                <div class="card" style="margin-bottom:10px">
                                    <div class="card-header text-center">
                                        <a href="{{ route('shopitems.show', $shop->id) }}">
                                            {{ $shop -> title }}
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        {{ $shop -> description }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @else
                        <div class="row justify-content-center"><h4>Нет магазинов в данной категории</h4></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
