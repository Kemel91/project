@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Категории магазинов</div>

                <div class="card-body">
                    @if (!$categories -> isEmpty())
                 <div class="row justify-content-center">
                        @foreach ($categories as $category)
                            <div class="col-md-4">
                                <div class="card" style="margin-bottom:10px">
                                    <div class="card-header text-center">
                                        <a href="{{ route('category.show', $category->id) }}">{{ $category -> title }}</a>
                                    </div>
                                    <div class="card-body">
                                        {{ $category -> description }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @else
                        <div class="row justify-content-center"><h4>Категории не созданы</h4></div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="row">
                <a class="btn btn-primary" href="{{ route('category.create') }}">Добавить категорию</a>
            </div>
        </div>
    </div>
</div>
@endsection
