@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Ваша корзина магазина <a href="{{ route('shopitems.show', $shop -> shop -> id) }}">{{ $shop -> shop -> title }}</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">Изо</th>
                            <th scope="col">Название</th>
                            <th scope="col">Кол-во</th>
                            <th scope="col">Цена</th>
                          </tr>
                        </thead>
                        <tbody>
                        @forelse ($paginator as $item)
                          <tr>
                            <td><img src="https://picsum.photos/id/{{ $item -> item_id }}/42"/></td>
                            <td>{{ $item -> items -> title }}</td>
                            <td>{{ $item -> count }}</td>
                            <td>{{ $item -> shopItems -> price }}</td>
                          </tr>
                        @empty
                          <tr><th>У вас нет заказов</th></tr>
                        @endforelse
                        </tbody>
                </table>
                <div class="row justify-content-center">
                    @include('vendor.pagination.simple-bootstrap-4')
                </div>                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection