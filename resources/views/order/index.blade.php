@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Ваша корзина
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Действия</th>
                            <th scope="col">Магазин</th>
                            <th scope="col">Кол-во заказов</th>
                          </tr>
                        </thead>
                        <tbody>
                        @forelse ($order_shops as $order)
                          <tr>
                            <td>{{ $order -> shop_id }}</td>
                            <td><a href="{{ route('order.show', $order -> id) }}">Просмотреть</a></td>
                            <td>
                                <a href="{{ route('shopitems.show', $order -> shop_id) }}">
                                    {{$order -> shop -> title }}
                                </a>
                            </td>
                            <td>{{ $order -> order_items_count }}</td>
                          </tr>
                        @empty
                          <tr><th>У вас нет заказов</th></tr>
                        @endforelse
                        </tbody>
                </table>                       
                </div>
            </div>
        </div>
    </div>
</div>
@endsection