@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row no-gutters">
                  <div class="col-md-4">
                    <img src="{{ $image }}" class="card-img" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title">{{ $item -> title }}</h5>
                      <p class="card-text">{{ $item -> description }}</p>
                      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                    <div class="card-body">
                        <a href="{{ back() -> getTargetUrl() }}" class="card-link">Назад</a>
                    </div>
                  </div>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection
