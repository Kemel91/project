@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            Добавить новый магазин
                        </div>
        
                        <div class="card-body">
                                <form method="POST" action="{{ route('shop.store') }}">
                                        @csrf
                
                                        <div class="form-group row">
                                            <label for="title" class="col-md-4 col-form-label text-md-right">Название</label>
                
                                            <div class="col-md-6">
                                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>
                
                                                @error('title')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="category_id" class="col-md-4 col-form-label text-md-right">Выберите категорию</label>
                
                                            <div class="col-md-6">
                                                <select name="category_id" class="form-control @error('category_id') is-invalid @enderror" required>
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category -> id }}">{{ $category -> title }}</option>
                                                    @endforeach
                                                </select>
                
                                                @error('category_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                                <label for="description" class="col-md-4 col-form-label text-md-right">Описание</label>
                    
                                                <div class="col-md-6">
                                                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" rows="8">{{ old('description') }}</textarea>
                    
                                                    @error('description')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                        <div class="form-group row mb-0">
                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Создать
                                                    </button>
                                                </div>
                                        </div>
                                </form>
                        </div>
                    </div>
            </div>
    </div>
</div>
@endsection