@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              @if (session('success'))
                <div class="alert alert-info alert-dismissible fade show text-center" role="alert">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
              @endif
                <div class="card-header">
                    <a href="{{ route('category.show',$shop->category_id) }}">Магазины</a> | {{ $shop -> title }}
                </div>
                <div class="card-body">
                        <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Изо</th>
                                    <th scope="col">Название</th>
                                    <th scope="col">Остаток</th>
                                    <th scope="col">Цена</th>
                                    <th scope="col">Добавить</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach ($paginator as $item)
                                  <tr @if($item->count < 10) style="background-color:#EDDBE4" @endif>
                                    <th scope="row">{{ $item -> item_id }}</th>
                                  <td><img src="https://picsum.photos/id/{{ $item -> item_id }}/42"/></td>
                                    <td><a href="{{ route('item.show', $item -> item_id) }}">{{$item -> getItem -> title }}</a></td>
                                    <td>{{ $item -> count }}</td>
                                    <td>{{ $item -> price }}</td>
                                    <td>
                                      <a href="{{ route('order.add', $item -> id ) }}">
                                        <img src="{{ asset('images/add.png') }}" alt="Добавить в корзину"/>
                                      </a>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                        </table>
                        <div class="row justify-content-center">
                            @include('vendor.pagination.simple-bootstrap-4')
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
