<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * @group register
 */
class AuthRegisterTest extends TestCase
{
    //use RefreshDatabase;
    /**
     * Exist Register Page
     *
     * @return void
     */
    public function testExistPage()
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
    }

    /**
     * All inputs required
     *
     * @return void
     */
    public function testDataIsRequired() {
        $response = $this -> from(route('register')) 
              -> post(route('register'),
              [
                  'name' => '',
                  'password' => '',
                  'email' => ''
              ]);
        $response -> assertSessionHasErrors(['name','password','email']);
    }

    /**
     * Name field is null
     *
     * @return void
     */
    public function testNameIsRequired() {
        $response = $this -> from(route('register')) 
              -> post(route('register'),
              [
                  'name' => '',
                  'password' => '12234455',
                  'email' => 'dsds@dd'
              ]);
        $response -> assertSessionHasErrors(['name']);
    }

    /**
     * Password field is min charasters
     *
     * @return void
     */
    public function testPasswordMinCharasters() {
        $response = $this -> from(route('register')) 
              -> post(route('register'),
              [
                  'name' => '11S',
                  'password' => '122343434',
                  'email' => 'dsds'
              ]);
        $response -> assertSessionHasErrors(['password']);
    }

    /**
     * Creating user after register
     *
     * @return void
     */
    public function testRegisterTrue() {
        $data = [
            'name' => '11S',
            'password' => '12345678',
            'email' => 'dsds@ds'
        ];
        $this -> assertDatabaseMissing('users', $data);
        $data['password-confirm'] = '12345678';
        $response = $this -> from(route('register')) 
              -> post(route('register'), $data);
       // $this -> assertDatabaseHas('users',$data);
       // $this -> assertNotEmpty(User::whereName('11S') -> get());
    }
    /**
     * Auth Test True
     */
    public function testAuthUser() {
        $user = User::find(1);
        $response = $this -> actingAs($user) -> get(route('profile.index'));
        $response -> assertStatus(200);
    }

    /**
     * Auth Test False
     */
    public function testAuthUserFalse() {
        $response = $this -> get(route('profile.index'));
        $response -> assertStatus(302);
    }
}
